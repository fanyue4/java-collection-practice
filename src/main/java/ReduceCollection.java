import java.util.List;

public class ReduceCollection {
    public static int getMax(List<Integer> list) {
        // Need to be implemented
        return list.stream().reduce(0, Math::max);
    }
    
    public static double getAverage(List<Integer> list) {
        // Need to be implemented
        // return getSum(list) / list.size();
        return list.stream().mapToDouble(element -> element).average().getAsDouble();
    }

    public static int getSum(List<Integer> list) {
        // Need to be implemented
        return list.stream().reduce(0, Integer::sum);
    }
    
    public static double getMedian(List<Integer> list) {
        // Need to be implemented
        /*
        if (list.size() % 2 != 0) {
            return list.get((list.size() + 1) / 2 - 1);
        } else {
            return (list.get(list.size() / 2 - 1) + list.get(list.size() / 2 + 1)) / 2;
        }
         */
        return list.stream().mapToDouble(element -> element).sorted()
                .skip(list.size() % 2 == 0 ? list.size() / 2 - 1 : (list.size() - 1) / 2)
                .limit(list.size() % 2 == 0 ? 2 : 1)
                .average()
                .getAsDouble();
    }
    
    public static int getFirstEven(List<Integer> list) {
        // Need to be implemented
        int result = list.stream().reduce(list.get(0), (a, b) -> a % 2 == 0 ? a : b);
        return result % 2 != 0 ? null : result;
    }
}
