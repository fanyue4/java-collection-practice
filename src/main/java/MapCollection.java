import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class MapCollection {
    
    public static List<Integer> doubleCollection(List<Integer> list) {
        // // Need to be implemented
        return list.stream().map(element -> element * 2).collect(Collectors.toList());
    }
    
    public static List<String> mapToStringCollection(List<Integer> list) {
        // Need to be implemented
        return list.stream().map(element -> Character.toString ((char)(element + 96))).collect(Collectors.toList());
    }
    
    public static List<String> uppercaseCollection(List<String> list) {
        // Need to be implemented
        return list.stream().map(String::toUpperCase).collect(Collectors.toList());
    }
    
    public static List<Integer> transformTwoDimensionalToOne(List<List<Integer>> list) {
        // Need to be implemented
        return list.stream().flatMap(Collection::stream).collect(Collectors.toList());
    }
}
