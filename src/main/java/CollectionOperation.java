import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CollectionOperation {
    
    public static List<Integer> getListByInterval(int left, int right) {
        // Need to be implemented
        /*
        List<Integer> list = new ArrayList<>();
        if (left <= right) {
            for (int i = left; i < right + 1; i++) {
                list.add(i);
            }
            return list;
        } else {
            for (int i = left; i > right - 1; i--) {
                list.add(i);
            }
            return list;
        }
         */
        /*
        return left <= right ? IntStream.range(left, right + 1).boxed().collect(Collectors.toList())
                : IntStream.range(right, left + 1).boxed().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
         */
        return IntStream.iterate(left, i -> left <= right ? ++i : --i)
                .limit(Math.abs(right - left) + 1)
                .boxed()
                .collect(Collectors.toList());
    }
    
    
    public static List<Integer> removeLastElement(List<Integer> list) {
        // Need to be implemented
        List<Integer> newList = new ArrayList<>(list);
        newList.remove(newList.size() - 1);
        return newList;
    }
    
    public static List<Integer> sortDesc(List<Integer> list) {
        // Need to be implemented
        // return list.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        list.sort(Comparator.reverseOrder());
        return list;
    }
    
    
    public static List<Integer> reverseList(List<Integer> list) {
        // Need to be implemented
        Collections.reverse(list);
        return list;
    }
    
    
    public static List<Integer> concat(List<Integer> list1, List<Integer> list2) {
        // Need to be implemented
        return Stream.concat(list1.stream(), list2.stream()).collect(Collectors.toList());
    }
    
    public static List<Integer> union(List<Integer> list1, List<Integer> list2) {
        // Need to be implemented
        return Stream.concat(list1.stream(), list2.stream()).distinct().sorted().collect(Collectors.toList());
    }
    
    public static boolean isAllElementsEqual(List<Integer> list1, List<Integer> list2) {
        // Need to be implemented
        return list1.size() == list2.size() && list2.containsAll(list1);
    }
}
